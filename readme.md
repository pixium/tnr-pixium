
## Installation

Use the package manager [pip](https://pip.pypa.io/en/stable/) to install libraries.

```bash
pip install os
pip install time
pip install mime
pip install selenium
pip install atlassian-python-api
pip install requests
```

## Configuration 
The file config.json is the file you must modify for all the variables
### Config.json
* link_to_driver is the path to the chrome driver use by selenium. You must have the same version of the driver that your version of Chrome. To check your chrome version (chrome://settings/help). To download the selenium driver go to this [link](https://chromedriver.chromium.org/downloads) 
* username is your username that you use to connect to confluence (The user must be a member of pixium qualité, and pixium qualité 2)
* password is your password that you use to connect to confluence (The user must be a member of pixium qualité, and pixium qualité 2)
* base_url is the url of your confluence site
* creation_form_url is the url to the creation form in space when you want to run the test
* space_key is the space_key in which you want to run the test
* notification_group is the parameter for the worfklow PQSD
* nb_occurences is the number of times a single user must do the cycle defined in the test
* nb_users is the number of users you want to run the tests
## Usage
```python

# create 75 users
python create_users.py

# Simulate the activty of the users
python tests.py

```