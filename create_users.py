
import time
import json
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.common.exceptions import TimeoutException

with open("config.json") as config_file:
        data = json.load(config_file)

base_url= data['base_url']
username = data['username']
password = data['password']

TIMEOUT_SECONDS = 60


def login(driver,username, password,base_url):
    try:
        driver.get(base_url + 'login.action')
        time.sleep(1)
        field_username = driver.find_element_by_id('os_username')
        field_username.send_keys(username)

        field_password = driver.find_element_by_id('os_password')
        field_password.send_keys(password)

        button_add_module = driver.find_element_by_id('loginButton')
        button_add_module.click()

        driver.implicitly_wait(10)
        print(driver.current_url)
        if(driver.current_url) == base_url + "#all-updates":
            print("Login successful as " + username)
            return True
        else:
            print("Login has failed")
            return False
    except Exception as e:
        print(e)
        print("Login has failed")
        return False

def is_element_appeared(driver,element_Xpath,by_what, timeout = TIMEOUT_SECONDS):
    try:
        WebDriverWait(driver, timeout).until(EC.visibility_of_element_located((by_what, element_Xpath)))
        return True
    except TimeoutException:
        raise RuntimeError("Error with :" + element_Xpath)
        return False
def is_element_clickable(driver,element_Xpath,by_what, timeout = TIMEOUT_SECONDS):
    try:
        WebDriverWait(driver, timeout).until(EC.element_to_be_clickable((by_what, element_Xpath)))
        return True
    except TimeoutException:
        raise RuntimeError("Click error with :" + element_Xpath)
        return False



def create_n_users(n_users):
    driver = webdriver.Chrome(data['link_to_driver'])
    login(driver,username,password,base_url)
    for counter in range(n_users) :
        if(is_element_appeared(driver,"admin-menu-link",By.ID)) :
            driver.find_element(By.ID,"admin-menu-link").click()
        if(is_element_appeared(driver,"manage-users-link",By.ID)) :
            driver.find_element(By.ID,"manage-users-link").click()
        if(is_element_appeared(driver,"password",By.ID)) :
            driver.find_element(By.ID,"password").send_keys(password)
        if(is_element_appeared(driver,"authenticateButton",By.ID)) :
            driver.find_element(By.ID,"authenticateButton").click()
        if(is_element_appeared(driver,"create-tab-link",By.ID)) :
            driver.find_element(By.ID,"create-tab-link").click()
        if(is_element_appeared(driver,"username",By.ID)) :
            driver.find_element(By.ID,"username").send_keys("testeur_n" + str(counter))
            driver.find_element(By.ID,"fullname").send_keys("testeur_n" + str(counter))
            driver.find_element(By.ID,"email").send_keys("confluencetestmailingadn@gmail.com")
            driver.find_element(By.ID,"password").send_keys("password_n" + str(counter))
            driver.find_element(By.ID,"confirm").send_keys("password_n" + str(counter))
        if(is_element_appeared(driver,"//input[@value='Add']",By.XPATH)) :
            driver.find_element(By.XPATH,"//input[@value='Add']").click()
        if(is_element_appeared(driver,"//a[contains(text(),'Edit Groups')]",By.XPATH)) :
            driver.find_element(By.XPATH,"//a[contains(text(),'Edit Groups')]").click()
        ##Add groups  
        if(is_element_appeared(driver,"pixium qualité",By.ID)) :
            driver.find_element(By.ID,"pixium qualité").click()
        if(is_element_appeared(driver,"pixium qualité 2",By.ID)) :
            driver.find_element(By.ID,"pixium qualité 2").click()
        if(is_element_appeared(driver,"save-btn1",By.ID)) :
            driver.find_element(By.ID,"save-btn1").click()
        ##Drop admin access
        if(is_element_appeared(driver,"websudo-drop",By.ID)) :
            driver.find_element(By.ID,"websudo-drop").click()
    driver.close()

if __name__ == '__main__':
    create_n_users(75)