import json
import time 
import mimetypes
import requests
from requests.packages.urllib3.exceptions import InsecureRequestWarning

requests.packages.urllib3.disable_warnings(InsecureRequestWarning)


from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.common.exceptions import TimeoutException
from atlassian import Confluence

from multiprocessing import Pool
from itertools import repeat



with open("config.json") as config_file:
        data = json.load(config_file)

base_url= data['base_url']
username = data['username']
password = data['password']
creation_form_url = data['creation_form_url']
space_key = data['space_key']
notification_group = data["notification_group"]
MAX_ITERATIONS = data["nb_occurences"]
NB_USERS = data["nb_users"]
TIMEOUT_SECONDS = 60

confluence = Confluence(
    url= base_url,
    username=username,
    password=password,
    verify_ssl= False
)

def login(driver,username, password,base_url):
    try:
        driver.get(base_url + 'login.action')
        time.sleep(1)
        field_username = driver.find_element_by_id('os_username')
        field_username.send_keys(username)

        field_password = driver.find_element_by_id('os_password')
        field_password.send_keys(password)

        button_add_module = driver.find_element_by_id('loginButton')
        button_add_module.click()

        driver.implicitly_wait(10)
        print(driver.current_url)
        if(driver.current_url) == base_url + "#all-updates":
            print("Login successful as " + username)
            return True
        else:
            print("Login has failed")
            return False
    except Exception as e:
        print(e)
        print("Login has failed")
        return False

def is_element_appeared(driver,element_Xpath,by_what, timeout = TIMEOUT_SECONDS):
    try:
        WebDriverWait(driver, timeout).until(EC.visibility_of_element_located((by_what, element_Xpath)))
        return True
    except TimeoutException:
        raise RuntimeError("Error with :" + element_Xpath)
        return False
def is_element_clickable(driver,element_Xpath,by_what, timeout = TIMEOUT_SECONDS):
    try:
        WebDriverWait(driver, timeout).until(EC.element_to_be_clickable((by_what, element_Xpath)))
        return True
    except TimeoutException:
        raise RuntimeError("Click error with :" + element_Xpath)
        return False


def upload_file_to_confluence(filename,page_id) :
    try :
        filename = filename.replace("\\","/")
        print(page_id)
        url = base_url + 'rest/api/content/' + \
        str(page_id) + '/child/attachment/'
        headers = {'X-Atlassian-Token': 'no-check'} #no content-type here!
        content_type, encoding = mimetypes.guess_type(filename)
        if content_type is None:
            content_type = 'multipart/form-data'
        print(filename)
        print(content_type)
        files = {'file': (filename, open(filename, 'rb'), content_type)}
        auth=(username,password)
        r = requests.post(url, headers=headers, files=files, auth=auth)
        print(filename)
        print(r.status_code)
        return r.status_code
    except Exception as e :
        print(str(e))

def chain_action_for_one_user(username_testeur,password_testeur,counter):
    driver = webdriver.Chrome(data['link_to_driver'])
    login(driver,username_testeur,password_testeur,base_url)
    driver.get(creation_form_url)
    #CreatePage
    page_name = "Page of " + username_testeur + " iteration " + str(counter)
    if(is_element_appeared(driver,"Titleofyourpage",By.ID)) :
        driver.find_element(By.ID,"Titleofyourpage").send_keys(page_name)
    if(is_element_appeared(driver,"formsSubmitButton",By.ID)) :
        driver.find_element(By.ID,"formsSubmitButton").click()


    #Get to page just created
    time.sleep(5)
    driver.get(base_url + "display/" + space_key + "/" + page_name)

    #Update the page
    if(is_element_appeared(driver,'editPageLink', By.ID)) :
        driver.find_element(By.ID,'editPageLink').click()
    if(is_element_clickable(driver,"rte-button-publish", By.ID)) :
        driver.find_element(By.ID, "rte-button-publish").click()

    #Add attachments to the page

    page_id = confluence.get_page_id(space_key,page_name)
    if(is_element_appeared(driver,'editPageLink', By.ID)) :
        upload_file_to_confluence("files_to_upload/uploadWord.docx",page_id)
        upload_file_to_confluence("files_to_upload/uploadPdf.pdf",page_id)
        upload_file_to_confluence("files_to_upload/image.jpg", page_id)
        upload_file_to_confluence("files_to_upload/uploadExcel.xlsx", page_id)

    #Publish the page
    pageData = {"name":"Published","comment":"Run with migration","parameters" : [{"id":"param1366856668","value":username},
    {"id":"param480404873","value":notification_group}]}


    r = requests.put(base_url + '/rest/cw/1/content/'+ page_id+ '/state',
    data=json.dumps(pageData),
    auth=(username,password),
    verify = False,
    headers=({'Content-Type':'application/json'}))

    driver.close()



def recursive_users(nb_users,counter) :
    if(counter == MAX_ITERATIONS) :
        return
    recursive_users(nb_users ,counter+1)
    chain_action_for_one_user("testeur_n" + str(nb_users), "password_n" + str(nb_users),counter)





if __name__ == '__main__':
    p = Pool(5)
    args = []
    for counter in range(NB_USERS) :
        args.append(counter)
    p.starmap(recursive_users, zip(args,repeat(0)))





